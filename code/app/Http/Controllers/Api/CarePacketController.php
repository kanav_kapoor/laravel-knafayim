<?php

namespace App\Http\Controllers\Api;

use App\CarePacket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Support\Responses\APIResponse;

class CarePacketController extends Controller
{
    use APIResponse;

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validateData($request);

        CarePacket::create($data);

        return $this->successResponse('Care Packet Created Successfully');
    }

    /**
     * Valdiate Care Packet Data
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    private function validateData($request)
    {
        $data = $request->validate([
            'name'          => 'required|between:2,255',
            'address'       => 'required|between:2,255',
            'city'          => 'required|between:2,255',
            'state'         => 'required|between:2,255',
            'zip'           => 'required|between:2,255',
            'email'         => 'required|between:2,255|email',
            'message'       => 'required|between:2,1000',
            'is_subscribed' => 'nullable|boolean'
        ]);

        if(!$data['is_subscribed']){
            $data['is_subscribed'] = false;
        }

        return $data;
    }
}
