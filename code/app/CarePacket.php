<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarePacket extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'address',
        'city',
        'state',
        'zip',
        'email',
        'message',
        'is_subscribed'
    ];
}
