
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import VeeValidate from 'vee-validate';

window.Vue = require('vue');
Vue.use(VeeValidate);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('lock-scroll', require('./components/LockScroll.vue'));
Vue.component('accordion-menu', require('./components/RecursiveAccordion.vue'));
Vue.component('mobilemenu', require('./components/MobileMenu.vue'));
Vue.component('h-2', require('./components/H2.vue'));
Vue.component('back-to-top', require('./components/BackToTop.vue'));
Vue.component('hero', require('./components/Hero.vue'));
Vue.component('feature-circle', require('./components/FeatureCircle.vue'));
Vue.component('feature-card', require('./components/FeatureCard.vue'));
Vue.component('cta', require('./components/CTA.vue'));
Vue.component('imagesection', require('./components/ImageSection.vue'));
Vue.component('kbutton', require('./components/Button.vue'));
Vue.component('navigation', require('./components/Header.vue'));
Vue.component('appfooter', require('./components/Footer.vue'));
Vue.component('location', require('./components/Location.vue'));
Vue.component('city-contact', require('./components/CityContact.vue'));
Vue.component('faqs', require('./components/FAQs.vue'));
Vue.component('accordion-item', require('./components/AccordionItem.vue'));
Vue.component('request-a-package', require('./components/RequestAPackage.vue'));
Vue.component('vee-error', require('./components/VeeError.vue'));

const app = new Vue({
    el: '#app',
    data: {
		},
	});
