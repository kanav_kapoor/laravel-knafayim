<div class="banners">
	<div class="banner relative">
		<div class="hero bg-home-hero bg-fixed xl:bg-center bg-cover">
			<div class="h-full flex items-center justify-start xl:justify-end xl:items-start ml-12 xl:ml-0">
				<p class="text-white text-2xl lg:text-3xl xl:text-5xl font-bold mt-4 mr-4 md:mt-12 md:mr-12 xl:mt-24 xl:mr-24">We lift you on wings of <br />hope so you can fly.</p>
			</div>
			<section class="divider-purple absolute pin-b flex flex-col items-center z-0 pb-12">
				<h2 class="text-white text-center w-screen text-3xl md:5xl my-6 leading-normal">KNAFAYIM</h2>
				<p class="text-white text-center w-full px-6 lg:p-0 lg:w-2/5 text-sm lg:text-xl leading-normal">The premiere organization strengthening and supporting families facing pregnancy and early baby loss.</p>
			</section>
		</div>
	</div>
</div>
