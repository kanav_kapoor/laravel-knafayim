<section class="tele w-screen flex flex-col-reverse lg:flex-row bg-secondary-accent bg-gradient-teal-right pt-16 pb-0 lg:py-16 px-6 md:px-12">
	<div class="content flex flex-col items-start justify-center w-1/2 portrait-full text-center lg:text-left">
		<h2 class="text-white text-2xl font-bold leading-normal w-full"> What the Mind Cannot Compute</h2>
		<div class="description text-white text-basic lg:text-lg w-full lg:w-3/4 leading-normal mt-6 mb-4">
          Finding Meaning In Loss<br>
            Twice a year Knafayim presents a series of eight teleconferences, moderated by professionals to address women in grief. It is a place where questions are answered, stories are shared, and women are strengthened.
        </div>
        <a href="#" class="bg-primary-light leading-loose hover:bg-primary text-white text-sm shadow font-medium py-1 px-6 rounded-full no-underline mt-4 lg:my-8 mx-auto lg:mx-0">LISTEN</a>
	</div>
	<div class="w-full lg:w-1/2 mb-16">
		<div id="placeholder" class="bg-white shadow-md w-full lg:w-3/4 mx-auto my-0" style="height: 40vmin;"></div>
	</div>
</section>
<div class="divider">
	<svg id="bigHalfCircle" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" viewBox="0 0 100 100" preserveAspectRatio="none">
		<defs>
			<linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="-1%">
				<stop offset="60%" style="stop-color:rgba(31,194,181,1);stop-opacity:1" />
				<stop offset="100%" style="stop-color:rgba(178,254,247,1);stop-opacity:1" />
			</linearGradient>
		</defs>
	    <path class="diff" d="M0 100 C40 0 60 0 100 100 Z" fill="url(#grad1)"></path>
	</svg>
</div>
