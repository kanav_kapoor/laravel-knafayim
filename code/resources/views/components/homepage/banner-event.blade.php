<section class="w-screen flex w-full flex-col lg:flex-row justify-center py-6 bg-center">
	<div class="banner-event text-center w-full lg:w-1/2 relative bg-no-repeat bg-cover">
    <h2 class="relative z-2 text-4xl font-bold uppercase text-white mt-24">Was It For Nothing?</h2>
	</div>
  <div class="w-full lg:w-1/2 p-16 text-center">
  <h2 class="text-2xl mb-2 block uppercase text-secondary-accent">Knafayim</h2>
  <h3 class="text-lg mb-6 block">presents</h3>
  <h4 class="relative z-2 text-2xl font-bold uppercase mb-6 text-primary">Was It For Nothing?</h4>
  <span class="text-xl mb-2 block">A musical event where mothers hearts tunes and tales are shared.</span>
  <p class="text-normal leading-loose block mb-10">
    This event is for mother's who have experienced baby loss, stillbirth, and multiple loss.
  </p>
  <div class="mb-6 px-6 py-2">
    <h3 class="font-light text-xl">Sunday</h3>
    <span class="text-2xl font-bold text-primary mb-4">December 30</span>
    <span class="block mb-2 font-bold text-lg italic underline">8:30pm</span>
  </div>
  <div class="flex justify-center mb-12">
    <div class="p-6 mx-2">
      <h3 class="mb-2 font-light">Moderated By:</h3>
      <span class="text-2xl font-bold italic underline">Malkie Klaristenfeld</span>
    </div>
    <div class="p-6 mx-2">
      <h3 class="mb-2 font-light">Special guest speaker:</h3>
      <span class="text-2xl font-bold font-bold italic underline">Rabbi Dr. Fox</span>
    </div>
  </div>
  <div class="mt-10 px-6 text-xs leading-loose">
  To register and for more information please text or call Rochel Lea at <a href="tel:9176915470">9176915470</a> or email at <a href="mailto:support@knafayimwings.org">support@knafayimwings.org</a>
  </div>
</div>
</section>
