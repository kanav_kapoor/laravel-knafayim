<section class="w-screen flex flex-col items-center">
	<div class="text-primary text-3xl font-bold leading-normal my-12">Upcoming Events</div>
  @include('components.homepage.banner-event')
	<div class="w-full flex flex-row items-center justify-center flex-wrap">
		<div class="w-full lg:w-1/3 p-8">
			@include('components.general.event')
		</div>
		<div class="w-full lg:w-1/3 p-8">
			@include('components.general.event')
		</div>
		<div class="w-full lg:w-1/3 p-8">
			@include('components.general.event')
		</div>
	</div>
	<button class="text-lg font-semibold rounded-full px-6 py-2 my-10 leading-normal bg-white border border-purple text-purple hover:bg-purple hover:text-white">See All Events</button>
</section>
