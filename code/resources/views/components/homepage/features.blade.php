<section class="features w-screen flex w-full flex-col lg:flex-row items-center justify-center py-24 bg-center bg-no-repeat bg-cover">
	<div class="w-full lg:w-1/3 p-8">
		<feature-circle title="Support" link="#">
			You have experienced a loss; your dream has become a nightmare. Support enables one to weather the storm. We are here for you.
		</feature-circle>
	</div>
	<div class="w-full lg:w-1/3 p-8">
		<feature-circle title="Forum" link="#">
			My friend, you are not alone. This is a journey one cannot traverse without the help of others. You may need validation, understanding, information, and hope.
		</feature-circle>
	</div>
	<div class="w-full lg:w-1/3 p-8">
		<feature-circle title="Education" link="#">
			You have so many questions and so much fear to navigate the territory. Ask and learn of news and updates in the field.
		</feature-circle>
	</div>
</section>
