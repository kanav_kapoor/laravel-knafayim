<div class="fadein-white">
	<img class="hide-landscape" src="/img/IMG_8126.JPG" />
</div>
<section class="care-packet w-screen flex bg-50% bg-no-repeat">
	<div class="content flex flex-col items-start justify-center w-1/2 px-6 py-6 md:px-12 my:pt-12 lg:py-32 portrait-full">
		<h2 class="title text-secondary-accent text-3xl font-bold my-4">Care Packets</h2>
		<h3 class="subtitle text-primary text-2xl font-extrabold my-4"></h3>
		<p class="description text-primary text-sm lg:text-basic leading-normal">Knafayim cares. Our packets are assembled with love and empathy and filled with lots of information. Each packet includes a magazine filled with chizuk and guidance that are useful during the period following a loss. A musical CD warms the heart and infuses a grieving mother with strength and hope. And you can always count on chocolate for comfort!</p>
		<a href="/request-a-package" class="care-button bg-teal leading-loose hover:bg-teal-dark text-white text-sm shadow font-medium py-1 px-6 rounded-full no-underline my-8">ORDER NOW</a>
	</div>
	<div class="transparent-overlay w-1/2 hide-portrait"></div>
</section>
