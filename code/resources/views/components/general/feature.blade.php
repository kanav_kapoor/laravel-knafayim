<h2 class="feature-title text-white text-xl">
  {{ $title }}
</h2>
<div class="featurebox-holder w-full">
  <div class="box rounded-full w-full h-full text-center flex-col items-center justify-center bg-black lg:bg-teal hover:bg-black relative border-primary border:0 lg:border-8 hover:border-0 mx-auto">
    <div class="pseudo-center content">
      <h2 class="text-white">{{$title}}</h2>
      <p class="text-white text-sm w-3/4 mx-auto my-8">
        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
      </p>
      @include('components.general.button-green', ['text' => 'GO', 'link' => '#'])
    </div>
  </div>
</div>
