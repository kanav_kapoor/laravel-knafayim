<a href="{{$link}}" class="bg-teal leading-loose hover:bg-teal-dark text-white text-base shadow font-medium py-2 px-6 rounded-full no-underline my-2">{{$text}}</a>
