<div class="cursor-pointer bg-white mx-auto max-w-sm shadow-md hover:shadow-lg rounded-lg overflow-hidden">
  <div class="sm:flex sm:items-center px-6 py-4">
    <img class="block h-16 sm:h-24 rounded-full mx-auto mb-4 sm:mb-0 sm:mr-4 sm:ml-0" src="https://randomuser.me/api/portraits/women/85.jpg" alt="">
    <div class="text-center sm:text-left sm:flex-grow">
      <div class="mb-4">
        <p class="text-xl leading-tight">12/08</p>
        <p class="text-sm leading-tight text-grey-dark">15:00</p>
      </div>
      <div>
        <span class="text-xs font-semibold py-1 leading-normal bg-white text-primary text-primary-dark">Educational Seminar</span>
      </div>
    </div>
  </div>
</div>
