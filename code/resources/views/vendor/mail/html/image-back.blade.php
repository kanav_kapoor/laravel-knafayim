<tr>
	<td align="center" class="image-background" style="background-image: url('{{$image}}'); background-repeat: no-repeat; background-position: top center; background-size: cover;">
	    {{ $slot }}
	</td>
</tr>