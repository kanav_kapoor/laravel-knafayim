@extends('layouts.app')

@section('content')
	<hero tagline="We are sorry you are experiencing a loss." url="/img/cares/sky-birds.jpg">
		Knafayim is here to take you under its wings and guide you through these trying times. We are in this together. Knafayim volunteers are all bereaved parents who’ve learned to fly.
	</hero>
	<section class="w-full text-center bg-teal-custom text-white py-16 px-4 lg:px-8">
		<h2 class="title text-3xl font-bold relative z-0">Knafayim Cares</h2>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-16 font-bold">
				After you have heard those words that seemed to topple your world, Knafayim is here to hold your hand. As part of our services, we provide:
			</p>
		</div>
		<div class="flex flex-wrap justify-center">
			<feature-card class="w-full md:w-1/2" title="Medical Coordiation">
				We provide care coordination, patient advocacy and medical referral services to provide available options and guide you to proper decisions and treatment.
			</feature-card>
			<feature-card class="w-full md:w-1/2" title="Family support">
				We provide family support including home care, guidance for children, and direction for the future. Post pregnancy loss, we offer private sessions for couples to discuss follow up care initiatives.
			</feature-card>
			<feature-card class="w-full md:w-1/2" title="Trained Doulas Treatment">
				Knafayim has trained doulas to be in the hospital with parents experiencing a loss. Our doulas provide physical, emotional, and informational support, holding your hand during labor and delivery. They visit during your hospital stay and follow up after you have been discharged.
			</feature-card>
		</div>
	</section>
	<section class="py-20 bg-white bg-center bg-cover text-center">
		<h-2 :large="true">WORLDWIDE SERVICES</h-2>
		<p class="leading-loose text-base lg:text-lg">We aim to provide services for families worldwide. Knafayim now has active volunteers in</p>
		<ul class="list-reset flex justify-center flex-wrap mt-16 mb-2">
			<location>Tri-state Area</location>
			<location>Toronto</location>
			<location>Israel</location>
			<location>Philadelphia</location>
			<location>Arizona</location>
		</ul>
		<p class="leading-loose text-base lg:text-lg text-secondary-accent font-bold">We do Skype calls for women in other communities who require support</p>
		<div class="border-b border-secondary-accent w-3/4 mx-auto my-8"></div>
		<h-2 class="my-20">For immediate assistance, contact your local Knafayim:</h-2>
		<ul class="list-reset flex justify-between flex-wrap my-16 mx-auto w-full md:w-3/4">
			<city-contact city="New York">(917)627-5528</city-contact>
			<city-contact city="Israel">(917)627-5528</city-contact>
			<city-contact city="Toronto">(917)627-5528</city-contact>
		</ul>
		<p class="leading-loose text-base lg:text-lg">For all other areas, contact us at <span class="text-teal-dark font-semibold">support@knafayimwings.org</span></p>
	</section>
	<cta link="#" button-text="FILL FORM" tagline="Discover The Power of Support" align="start">
		If you need someone to hold your hand, please complete the form below, and someone will connect with you within 24 hours.
	</cta>
@endsection
