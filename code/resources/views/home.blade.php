@extends('layouts.app')

@section('content')
	@include('components.homepage.banner-carousel')
	@include('components.homepage.knafayim-cares')
	@include('components.homepage.features')
	@include('components.homepage.teleconference')
	@include('components.homepage.recent-events')
	<cta link="#" button-text="JOIN NOW" tagline="Discover The Power of Support"></cta>
@endsection
