@extends('layouts.app')

@section('content')
	<hero fade-color="white" url="/img/cares/lotus.jpg">
	</hero>
	<section class="w-full text-center py-16 px-4 lg:px-8">
		<h2 class="title text-3xl font-bold relative z-0 text-secondary-accent">When Is a Miscarriage a Medical Crisis?</h2>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left text-primary">
			<p class="mb-16 font-bold">
				Miscarriage typically does not rise to the level of a medical emergency. Bleeding, cramping, fever and chills, nausea, vomiting, and diarrhea are all common symptoms; however, there is still cause to consider the risk of hemorrhage, anemia, high fever, and other symptoms suggesting endometritis or extreme distress.
			</p>
			<p class="mb-16">
				The following symptoms warrant emergency care:
			</p>
			<p class="mb-10">
				- Bleeding enough to soak through one heavy duty menstrual pad per hour for 2 consecutive hour
			</p>
			<p class="mb-10">
				- Fever of 100.4 degrees F (38 degrees C) for more than 4 consecutive hour
			</p>
			<p class="mb-10">
				- Bad smelling discharge
			</p>
			<p class="mb-10">
				- Pain considered intolerable by the patient despite use of OTC medications such as Acetaminophen
			</p>
			<p class="mb-10">
				- Blood clots the size of a plum
			</p>
			<h3 class="text-3xl mt-6 mb-12">
				Physiological Impact on Patients
			</h3>
			<p class="mb-16 font-bold">
				Miscarriage can be compared to the actual birthing process. As with labor at term, the amount and duration of pain, bleeding, cramping, nausea, and other symptoms experienced during a miscarriage will vary. Those providing care for the woman experiencing a miscarriage need to be aware that women will often experience fear, shock, uncertainty, and loss of control in the face of a miscarriage. This can be heightened by the intense physical symptoms they feel. Although intrauterine infection is rare with spontaneous miscarriage, surgical management increases the risk. Watch for signs of infection (fever, heavy bleeding, bad smelling discharge, or increasing pelvic pain) and consider antibiotic prophylaxis.
			</p>
			<p class="mb-16 font-bold">
				A return to normal levels of physical activity usually takes 3 to 4 weeks. Hormonal changes may lead to mood swings, shifts in sleeping and eating patterns, difficulty concentrating or focusing, and adjustments in your overall level of energy.
			</p>
			<h3 class="text-3xl mt-6 mb-12">
				Postpartum depression can also be experienced post miscarriage.
			</h3>
			<p class="mb-16">
				Symptoms of postpartum depression include:
			</p>
			<p class="mb-10">
				- feeling numb, invisible, disconnected, or empty
			</p>
			<p class="mb-10">
				- thinking life is meaningless or irrelevant
			</p>
			<p class="mb-10">
				- thinking life is meaningless or irrelevant
			</p>
			<p class="mb-10">
				- dramatic changes in the ability to concentrate, eat, sleep, and/or perform routine tasks
			</p>
			<p class="mb-10">
				- feeling worthless or hopeless
			</p>
			<p class="mb-10">
				- having disturbing and/or intrusive thoughts
			</p>
			<p class="mb-32">
				- thinking about harming one’s self or others
			</p>
		</div>
	</section>
@endsection
