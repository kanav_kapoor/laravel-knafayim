@extends('layouts.app')

@section('content')
	<hero fade-color="white" url="/img/cares/bookmarks.jpg">
	</hero>
	<section style="margin-top: -6rem;" class="w-full text-center py-16 px-4 lg:px-8 overflow-visible relative pb-64">
		<h2 class="title text-3xl font-bold relative z-0 text-secondary-accent">Definition of a Miscarriage</h2>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left text-primary">
			<p class="mb-16 font-bold">
				In these clinical guidelines, provided by The American College of Obstetricians and Gynecologists (ACOG) in the 2015 Practice Bulletin that described miscarriage as “a non-viable, intrauterine pregnancy with either an empty gestational sac or a gestational sac containing an embryo or fetus without fetal heart activity”
			</p>
			<h3 class="text-primary font-bold mb-8">Types of Miscarriages</h3>
			<faqs />
		</div>
	</section>
@endsection
