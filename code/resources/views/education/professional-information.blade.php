@extends('layouts.app')

@section('content')
	<hero fade-color="white" url="/img/cares/dandelion.jpg">
		<p class="text-3xl text-white font-bold mb-2">
			Healing the wounds of pregnancy loss
		</p>
		<p class="text-xl text-white mb-12">
			Pregnancy loss creates a unique type of grief, and psychotherapists say more should be done to support those facing such pain
		</p>
	</hero>
	<section style="margin-top: -12rem;" class="w-full text-center py-16 px-4 lg:px-8 overflow-visible relative pb-64">
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left text-primary">
			<p class="mb-16 font-bold">
				Losing a child is one of the most traumatic experiences most people can imagine. Losing a pregnancy, on the other hand, is too often met with the societal equivalent of a shrug.

				Perinatal loss is common—an estimated 10 percent to 20 percent of recognized pregnancies end in miscarriage, typically defined as the end of a pregnancy up to 20 weeks of gestation. A further 1 percent of pregnancies are lost to stillbirth, which occurs after 20 weeks. Such losses, though common, are often invisible. Many miscarriages occur early in pregnancy, before a woman has told friends or family members she’s expecting. Even when loved ones know about the pregnancy, people often fail to recognize the depth of the loss.
			</p>
			<img src="/img/education/baby-cot.jpeg" class="my-20" />
			<p class="mb-16">
				"It is often a profound experience that isn’t truly seen by society, as it is largely considered a taboo subject," says Rayna Markin, PhD, a licensed psychologist and associate professor at Villanova University who specializes in pregnancy loss and maternal mental health. Markin says that following the loss of a pregnancy, women often experience anguish and desperation, as well as feelings of shame and

				inadequacy. "I had one patient say she felt like a walking beehive—everything stung," she recalls. "Another said she felt like she’d lost a layer of skin and was walking around exposed."

				<br /><br />To better help such grieving clients, Markin served as guest editor for a special section in the journal Psychotherapy on psychotherapy for pregnancy loss, to which several leading experts in the field contributed (Psychotherapy, Vol. 54, No. 4, 2017).

				<br /><br />In the introduction to the section, Markin cites a variety of research to characterize the scale of the problem: As many as a quarter of women who experience a pregnancy loss have lasting adjustment problems. Some research suggests that up to 30 percent of pregnancy losses are followed by significant emotional reactions. And one in 10 women exhibit signs of a diagnosable disorder such as anxiety, depression or post-traumatic stress disorder following a reproductive loss. Given such strong emotional responses, Markin says, "one would think we’d have tons of psychotherapy studies on how we can help grieving parents, but we really don’t."

				<br /><br />She hopes the special issue will begin to close that gap. "We’re trained in society to be blind to this loss and not acknowledge it as legitimate, so we lack a language for even talking about it with patients," she says. "Our goal is to raise awareness and recognize the unique therapy needs of these patients."
			</p>
			<div>
				<h3 class="text-primary mb-4">
					Unmet hopes
				</h3>
				<p class="mb-10">
					Commonly used psychotherapeutic tools for addressing grief are helpful when counseling patients after a pregnancy loss, but this grief experience is unique in several ways.
				</p>
				<img src="/img/education/baby-shoes.jpg" class="my-20" />
				<p class="mb-10">
					One of the biggest differences is that losing a pregnancy is a loss of the future rather than of the past, says Janet Jaffe, PhD, a clinical psychologist and co-founder of the Center for Reproductive Psychology in San Diego, and a contributor to the special section. "As painful as it is to lose a loved one, you still have memories of that person. You can look at pictures and share stories," she says. "With a pregnancy loss, you only have what’s in your imagination. The story you have in your head about that future child just vanishes."

					<br /><br />Parents, and especially pregnant women, often feel a bond with the developing fetus early on. Yet there’s no accepted way to mark that loss. There’s generally no funeral or ceremony. Friends and family members as well as medical professionals often expect the person to move on quickly from the loss.

					<br /><br />"Even among physicians and medical staff there’s a lack of recognition about what the patient is experiencing emotionally," says Karen Hall, PhD, a San Diego-based psychologist specializing in infertility counseling and pregnancy loss who was not involved in the Psychotherapy special section. As a result, people may feel that they don’t have the right to mourn their loss, which can lead to feelings of isolation.

					<br /><br />Many women also experience a sense of failure or shame that doesn’t happen with other types of grieving, Hall adds. "It can turn quickly into complicated grief.
				</p>
			</div>
			<div>
				<h3 class="text-primary mb-4">
					Rewriting the story
				</h3>
				<p class="mb-10">
					To help clients through such grief, Jaffe, with her Center for Reproductive Psychology co-founders Martha Diamond, PhD, and David Diamond, PhD, endorse an approach they call the "reproductive story."
					<br /><br />
					"We all grow up with ideas about parenting, whether we choose to be parents or not," says David Diamond, who also holds a faculty position at the California School of Professional Psychology at Alliant International University in San Diego. "You have a reproductive story, conscious or unconscious, and when something goes wrong with this set of expectations and ideas and dreams, you can feel like you’ve lost more than a fetus or a baby. You’ve lost part of yourself." In a paper in the special section, Jaffe details how she uses the reproductive story concept to support clients through their losses. When clients discover how their deeply held beliefs about parenthood were disrupted, they can begin to understand that their feelings are normal, and stop blaming themselves for what they see as a failure, she explains. "One of the things that is wonderful about the concept of the reproductive story is that our patients get it immediately. It helps them feel understood, and can really help them to say, ‘OK, if this is my story, I am in control.’"
					<br /><br />
					The reproductive story model can also help people transition to parenthood following a pregnancy loss, Jaffe and her colleagues say. It’s a common misconception that becoming pregnant again can ease the pain of the previous loss, but it’s not always so simple.
					<br /><br />
					On the contrary, several studies have shown that women who experienced miscarriage or stillbirth had higher rates of anxiety and depression in a subsequent pregnancy. Most of that research dates back to the 1980s and 1990s, but a more recent study suggests that, for some women, such feelings persist during and even beyond a subsequent pregnancy—particularly if they have experienced multiple perinatal losses. Emma Robertson Blackmore, PhD, at the University of Rochester Medical Center, and colleagues, studied more than 13,000 women in England, 21 percent of whom reported previous miscarriages or stillbirths.
					<br /><br />
					They found that at 18 weeks of gestation, about 13 percent of women with no history of pregnancy loss experienced symptoms of depression, compared with 14 percent of women who had experienced one miscarriage and nearly 20 percent of women with two prior miscarriages. That pattern continued for years after birth. At 33 months after the birth of a healthy baby, about 12 percent of women with no history of miscarriage showed symptoms of depression. That figure was about 13 percent for women with one prior loss and nearly 19 percent for women with two prior losses (British Journal of Psychiatry, Vol. 198, No. 5, 2011).
					<br /><br />
					Those outcomes could affect a mother’s ability to bond with her baby, as David Diamond and Martha Diamond describe in another paper in the special section. "Pregnancy loss may have ramifications for one’s identity and sense of self, which can have a lasting influence on attachment to subsequent children," David Diamond says. He believes the reproductive story model can help patients rewrite the narrative to repair their damaged sense of self. "Our goal is to give clinicians a framework for understanding what these losses mean to people," he says.				</p>
			</div>
			<div>
				<h3 class="text-primary mb-4">
					A sense of balance
				</h3>
				<p class="mb-10">
					Cognitive-behavioral therapy (CBT) is another common tool psychotherapists use for treating patients in the aftermath of a pregnancy loss. Amy Wenzel, PhD, a clinical psychologist and assistant professor at the University of Pennsylvania School of Medicine, contributed a paper to the special section describing the benefits of CBT for these patients.

					There’s a common misconception that CBT is all about changing negative beliefs, Wenzel says. Indeed, the "cognitive restructuring" component of CBT can help clients identify and reframe unhelpful thoughts, such as the fear that they will never become parents or the belief that the loss was the result of something they did wrong. Yet other components of CBT are equally important, if not more so, she says: The practice of mindfulness can help people sit with their feelings about the loss, so that those feelings begin to lose their power. And through the process of behavioral activation, psychotherapists can help grieving patients identify and participate in activities that bring them joy and a sense of meaning. Those activities can provide positive reinforcement and help ease depressive symptoms.
					<br /><br />
					"For many people, parent-hood is the most meaningful life transition that there is. If it’s not working out as planned, it’s really devastating," Wenzel says. "Finding other things that bring meaning to their lives is really important for bringing a sense of balance."

While much of the discussion around pregnancy loss centers around women, men, too, can experience psychological distress. "Because it intimately involves the woman’s body, women tend to get more attached [to a fetus] earlier in the pregnancy, so there are unique issues for women who lose a pregnancy," Markin says. "But men are often the forgotten mourners."
					<br /><br />
In a review of research on men’s experience following miscarriage, Martha Rinehart, PhD, then at the College of New Jersey, found men experience grief at similar rates as women, exhibiting a range of emotions including a sense of loss, sadness, anger and alienation. Yet studies suggest that compared with women, men are less likely to grieve openly and may feel the need to mask their own feelings in order to appear strong for their grieving partners (Psychotherapy: Theory, Research, Practice, Training, Vol. 47, No. 3, 2010).					<br /><br />
Men’s and women’s different coping styles can lead to relationship conflict after a pregnancy loss, both Hall and Jaffe say. "Men and women are often in such different places when the loss happens. Women are more likely to want to discuss what’s happening, and men typically want more distance and emotional control," says Hall. "In couples therapy, it’s helpful to open women’s eyes to the fact that the loss is affecting their partners, just in a different way. That alone can decrease the conflicts significantly.					<br /><br />
			</div>
		</div>
	</section>
@endsection
