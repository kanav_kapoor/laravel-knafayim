@extends('layouts.app')

@section('content')
	<hero fade-color="white" url="/img/cares/dandelion.jpg">
		<p class="text-3xl text-white font-bold mb-2">
			For Family & Friends
		</p>
		<p class="text-xl text-white mb-12">
			Start with Understanding
		</p>
	</hero>
	<section style="margin-top: -12rem;" class="w-full text-center py-16 px-4 lg:px-8 overflow-visible relative pb-64">
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left text-primary">
			<p class="mb-16 font-bold">
				The English terms sympathy and compassion go back to Greek and Latin words, respectively, which originally referred to a “common feeling,” something “felt together.”<br /><br /> For those who wish to provide true comfort to friends or relatives who have lost a baby or a pregnancy, the ancient meanings still ring true. Feelings – what is actually being experienced by the grieving parents and by you in relation to their bereavement – should be at the heart of your expression of sympathy.
			</p>
			<p class="mb-16">
				If you wish to interact with the bereaved in a meaningful way, you must start by putting yourself in their shoes and by trying to understand what the baby or potential baby meant to them and how the death is affecting and will continue to affect their lives.
Understand that the amount of time a baby has lived does not determine its importance as a human being or as an object of love. Understand that a new baby can never replace the child who was lost, and that the parents’ love for their dead child cannot be “saved” for the future or automatically redistributed among their living children. Understand also how important your involvement can be during this painful period of adjustment. There will be few times when you are needed more than you are needed now.
			</p>
			<p class="mb-10">
				The loss of a pregnancy or baby is different than the loss of a life lived. It is the loss of hopes and dreams; it’s the loss of months of expectations and planning; it’s months of love accumulated with nowhere to go.
			</p>
			<h3 class="text-primary mb-4">
				What Can I Say?
			</h3>
			<p class="mb-10">
				Many people are uncomfortable discussing death with bereaved parents because they are afraid of saying the wrong thing, because they assume the parents won’t want to talk about it, or because they are reluctant to “reawaken” painful feelings.

Grieving parents are living their pain every day. If they seem to avoid talking about their baby’s death, it may be that they are trying to protect you from the intensity of their feelings. In this case, saying something profound to comfort them may be less important than letting the parents know that you acknowledge their pain and are there to listen if they want to talk. Questions can be helpful as well. Ask how they are doing. Ask how you can be of help.

Here are some examples of helpful things you can say:

“I’m sad for you.”<br /><br />

“This must be hard for you.”<br /><br />

“I’m sorry.”<br /><br />

“I’m here and I want to listen.”<br /><br />

“I know this is a bad time for you, and I want to help.”<br /><br />

“How are you doing with all of this?”

“What can I do for you?”

Above all, stay in touch. If the parents reject your overtures at first, give them some space and try again in a few weeks or months.

The following are examples of advice bereaved parents can do without:

“Be brave. Don’t cry.”<br /><br />

“It’s time to put this behind you and get on with your life.”<br /><br />

“You shouldn’t question God’s will.”<br /><br />

“Stop feeling sorry for yourself.”<br /><br />

“You should get out more.”<br /><br />

“This happened for the best.”<br /><br />

“It’s all part of God’s plan.”<br /><br />

“Be thankful you have another child.”<br /><br />

“You’re young, you’ll have other children.”<br /><br />

“There was probably something wrong with the baby, anyway.”<br /><br />

“It wasn’t really a person yet.”<br /><br />

“It was sick anyway.”<br /><br />
			</p>
		</div>
	</section>
@endsection
