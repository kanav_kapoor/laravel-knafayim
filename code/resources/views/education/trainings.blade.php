@extends('layouts.app')

@section('content')
	<hero url="/img/education/board.jpg">
	</hero>
	<section class="w-full text-center bg-teal-custom text-white py-16">
		<div>
			<h2 class="title text-3xl font-bold relative">Trainings</h2>
			<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
				<p class="mb-12 font-bold">
					The loss of a pregnancy or baby is one of the most tragic events that can strike a family. Usually, this is unexpected and the parents are unprepared for the experience. Many times, it will be their first encounter with death, especially one so close to their hearts.<br />
				</p>
				<p class="mb-12 font-bold">
					As a caregiver, you are in a unique position to provide a level of care and sensitivity that can greatly affect the course of a family’s grieving process
				</p>
				<p class="mb-12 font-bold">
					Knafayim trains and guides professionals, care givers, doulas and midwives for improved care of Jewish women during births and aftercare to promote healthy grief and healing. We work to strengthen partnerships with hospitals, physicians, rabbis and community leaders, medical personnel, and general community members and educate them on how to support families going through loss
				</p>
			</div>
		</div>
	</section>
	<imagesection img="/img/education/table.jpg" title="Workshops">
		The loss of a pregnancy or baby is one of the most tragic events that can strike a family. Usually, this is unexpected and the parents are unprepared for the experience. Many times, it will be their first encounter with death, especially one so close to their hearts.<br />
	</imagesection>
	<imagesection img="/img/education/books.jpg" :invert="true" title="Community">
		Knafayim works to educate and to strengthen partnerships with hospitals, physicians, community rabbis and agencies to encourage outreach to better address the needs of bereaved parents. In the Tri-state area, Knafayim hosts events to increase pregnancy loss awareness and to educate professionals on how to best deal with bereaved parents of the Jewish community
	</imagesection>
	<cta link="#" button-text="JOIN NOW" tagline="Discover The Power of Support" align="start">
		To schedule a training in your area, please contact <span class="text-secondary-accent">trainings@knafayimwings.com</span> or submit the form below:
	</cta>

@endsection
