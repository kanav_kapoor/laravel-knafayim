@extends('layouts.app')

@section('content')
	<hero url="/img/education/board.jpg">
	</hero>
	<section class="w-full text-center bg-teal-custom text-white py-16">
		<div>
			<h2 class="title text-3xl font-bold relative">Men and Loss</h2>
			<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
				<p class="mb-12 font-bold">
					The loss of a baby or pregnancy can be one of the most devastating things to happen to a couple. The experience of pregnancy loss can be a very lonely one, especially if you and your wife have not come across it before. This is a topic rarely discussed so you may find both that you know very little about the subject – including how common it is – and that you don’t know anyone else who has been through it.

<br /><br />Friends, family and the wider society appear to think miscarriage and baby loss is owned by the woman and is not a male issue. They don't seem to think that males grieve. It is critical that men take the space and permission to experience their full range of emotional responses to the baby loss.

<br /><br />Normal responses to loss may range from mild through severe. Attachments to pregnancies begin at different points for different people. This fact transcends gender. There is no correct response to baby loss.

<br /><br />Therefore, creating your space to grieve or process loss on your own terms in a supportive setting can be of great benefit to a couple experiencing loss together.

<br /><br />The experience of pregnancy loss may leave both you and your partner feeling quite bewildered, struggling to make sense of what has happened and to cope with a whole range of emotions. As a bereaved father, however, it can be very difficult to have your own needs recognized and met. In the weeks that follow miscarriage, attention tends to be focused on the mother and the father’s feelings can be overlooked. You may be taken aside and asked how your partner is, while few people ever think to ask after you. Some people may feel uncomfortable asking a man about his feelings, but others may simply assume that you are less affected by what has happened. You may find that you are expected to hide your feelings in order to be strong for your partner. It is important to recognize that miscarriage is likely to affect you as well as your partner, even though it may be in different ways. Your emotions and needs are equally important and valid, whatever they are. There are no right or wrong feelings – everyone reacts individually and you may find that your feelings fluctuate from day to day and even from moment to moment. But you may find some of those feelings difficult to cope with and talk about.
				</p>
			</div>
		</div>
	</section>
	<imagesection img="/img/education/table.jpg" title="Workshops">
		Here are some of the emotions men may experience:

		<br />● Shock: at the turn of events, especially if there were no signs that anything was wrong

		<br />● Anger: at medical staff for not preventing it from happening; at the unfairness of it all

		<br />● Grief: a strong and perhaps unexpected sense of loss and bereavement

		<br />● Isolation: loneliness, especially if your partner seems to be shutting you out, or if others don’t seem to understand how you feel

		<br />● Guilt and failure: for what happened; for your partner’s emotional and physical trauma; perhaps for not being there when it happened

		<br />● Relief: after a period of uncertainty

		<br />● Helplessness and frustration: at your lack of control over events

		<br />● Loss of concentration: feeling overwhelmed by events and emotions

		<br />● Anxiety: about your partner’s emotional and physical state; about your relationship; about a future pregnancy

		<br />● Impatience: the urge to get back to normal; and to try for another pregnancy	</imagesection>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-12 font-bold">
				Some of these may sound familiar, some less so, but all are common. When a woman miscarries, people often assume that she will have a stronger sense of connection with the baby, and thus experience a deeper sense of loss, than her partner. This may be true. You might feel more upset by your wife’s distress than by the loss of the baby. You have not experienced the same physical and emotional changes caused by pregnancy hormones as she has, and might not yet have

				seen a scan or felt the baby kick. For these reasons or for others, you may not feel such an intensity of sadness, rather a sense of disappointment. You might even feel that your wife might be over-reacting. On the other hand, you may be overcome by a real sense of loss. If you did see the baby on an early scan or saw or felt movements later in pregnancy, he or she may be quite real to you. This might have been an especially precious pregnancy, perhaps conceived after years of fertility problems, so that the loss is especially acute. Some men are quite shocked at the level of grief that they feel even after an early miscarriage and find it hard to cope with. If this has happened to you, it may help to know that it doesn’t usually last with the same intensity.
			</p>
		</div>
	<imagesection img="/img/education/books.jpg" :invert="true" title="">
		Your wife’s reactions and needs may be very different from yours, although it may just be that you deal with or express your feelings in different ways. You may recognize some of the following reactions:

		<br />● Tears: She is very upset and cries frequently

		<br />● Talking: She wants to talk constantly about the miscarriage – or she doesn’t want to talk about it at all

		<br />● Anger: She seems unreasonably angry with you and perhaps with others too

		<br />● Guilt: She feels guilty that she’s let you down, that she’s not able to provide a baby

		<br />● Isolation: She thinks that you don’t understand, that nobody understands

		<br />● Feelings about another pregnancy: She wants to get pregnant again straight away – or she is terrified of another pregnancy.
	</imagesection>
	<section class="w-full text-center py-16 px-4 lg:px-8 overflow-visible relative pb-64">
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-12 font-bold">
				Those raw emotions will gradually give way to a sense of sadness and regret, and an acceptance of what has happened. The experience will become less overwhelming and even if the sense of loss never truly leaves you, you will learn to deal with it in your own way. A late loss can also be very upsetting, since at this stage your wife will have to go through labor in the same way as in a full term birth. Again, you may feel powerless, that there is nothing you can do to help
			</p>
			<h3 class="text-primary mb-4">
				Your relationship
			</h3>
			<p class="mb-12">
				Your relationship

				“We’ve been through so much together. I wouldn’t wish it on anybody, but it did bring us closer together."

				Despite the distress of the miscarriage, you and your wife may be able to support each other very well and find that this experience has brought you closer together. You may feel that it is only the two of you who truly feel the loss of the baby and can help each other through the bad times and this can deepen and strengthen your relationship.

				However, the loss of a baby can put a strain on even the closest relationships. Just when you need each other most, it may be difficult to offer each other support. You may both be upset but in different ways or at different times.

				Or it may be that you have very different feelings about the miscarriage, with one of you struggling to understand why life is not "back to normal" and why it is taking the other a long time to come to terms with the loss. This can cause a lot of tension and arguments at what is already a difficult and distressing time.
			</p>
			<h3 class="text-primary mb-4">
				Practicalities
			</h3>
			<p class="mb-12">
				After the loss, you are likely to be the one left to deal with the practicalities of life: passing on the bad news, making domestic arrangements while your partner recovers, caring for any other children. This can all take its toll on you, physically and emotionally, so it makes sense to accept offers of help with practical things as well as emotional support.

				You may find that some work colleagues are a source of support. Others, though, may not mention the loss, either because they don’t view miscarriage as a particularly upsetting event, or simply because they don’t know what to say.
			</p>
			<h3 class="text-primary mb-4">
				Coping with your feelings
			</h3>
			<p class="mb-10">
				Everyone’s experience will be different and highly individual, affected by how much this particular pregnancy means to them and what is going on in the rest of their life. The chances are that whatever your feelings, you are not alone and many other men have felt the same way.
			</p>
				Here are some of the things men have suggested to help you cope with your feelings:

				<br /><br />● Talk about it: Try to express your feelings to someone with whom you feel comfortable. This may be your wife, but if you feel you can’t talk to her, try a family member.

				<br /><br />● Gather information: Try to find out more about what has happened, what is happening and what is likely to happen in the future – from your doctor. You may not be able to get all the answers you would like, but clear information can restore some feeling of control.

				<br /><br />● Give it time: Dealing with pregnancy loss is a process and there is no set timetable for getting through it. Don’t be surprised if you have a bad day some time after you feel quite recovered. Feelings can come and go and if you have a bad day, it is likely that the next one will be better.

				<br /><br />● Be prepared: It is very common for feelings to resurface at specific times. Particularly significant dates can be the day your baby was due to be born and the anniversary of the miscarriage;

				<br /><br />● If you find yourself ‘stuck’ in the raw stages of grief and simply cannot move on, counselling can be very helpful in working out how to deal with this.

				<br /><br />● Being strong … If your partner turns to you for support, you may welcome the opportunity to comfort her, to put your own feelings to one side and to be strong until you are confident that she is coming to terms with the bereavement. You may feel that you need to shield her from others, screening visitors and phone calls. You might feel quite comfortable with the traditional strong, silent and supportive role often expected of men. You may genuinely feel less affected by the pregnancy loss or you may simply find that this is the best way to show concern and care for your partner. Or it may be that you don’t want to appear vulnerable and feel you need to keep a lid on your own emotions so that you can remain a ‘pillar of strength’.
				<p class="my-16">
					What you can do for both of you:
					<br /><br /><h3>Talk to each other:</h3> Talking is probably the most important thing that either of you can do. It can help you both come to terms with what has happened and to make sense of your situation. Really listening and sharing your feelings with one another can help each of you understand what the other is going through. Many women say that they need to talk about their experience and feelings over and over again and you may find that you need to do the same.

<br /><br /><h3>Acknowledge the loss:</h3> However early the loss, the baby may well have represented your hopes and dreams for the future. If so, try not to pretend s/he never existed or to play down the baby’s importance in your lives. Acknowledge the sense of loss and sadness you may both be feeling and don’t be afraid to cry or to see your partner cry – it’s sometimes just what’s needed. Accept the differences: Your and your wife’s feelings and reactions may well be different now or may change over time. For example, one of you may still be grieving while the other is ready to move on; one may remember anniversaries while the other forgets. Try to accept that difference is normal and that you can still support and understand each other. Use other resources: Family and friends, work colleagues and health professionals, support organizations, websites and information leaflets may all have something to offer. Pick what seems most useful and don’t be afraid to ignore what’s unhelpful. Take stock: Don’t be surprised if the miscarriage makes you re-examine all sorts of things about yourself, your relationship with your partner and your priorities. Accept that your relationship may be permanently affected by what has happened, but remember that you can influence whether it is for better or worse by communicating with and supporting each other.

In breaking the silence around pregnancy and infant loss, we need to remember that men suffer loss too. In helping you understand your own experience with grief we are making our families stronger.
				</p>
		</div>

	</section>
	<cta link="#" button-text="JOIN NOW" tagline="Discover The Power of Support" align="start">
		To schedule a training in your area, please contact <span class="text-secondary-accent">trainings@knafayimwings.com</span> or submit the form below:
	</cta>

@endsection
