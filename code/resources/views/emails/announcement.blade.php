@component('mail::message')
@component('mail::image-back', ["image" => asset("https://knafayimwingsorg-my.sharepoint.com/:i:/g/personal/mklaristenfeld_knafayimwings_org/EQ_KU5J9DttNq8ah_zFQfNQBMhEqzdzdzOGAoAoJoUp2RQ?e=29XPQO")])
@component('mail::center')
@component('mail::white')
<br>
<h1 style="font-size: 25px; text-align: center;">Knafayim</h1>
## Presents

<h1 style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; font-size: 19px; font-weight: bold; margin-top: 0; text-align: center; color: #fff !important;"><q>Was it for Nothing?</q></h1>

## {{ $announcement->subtitle }}

## Sunday, December 30, 8:00 PM in Brooklyn.
<br>
This event is for mothers who have experienced baby loss, stillbirth, and multiple losses.

@component('mail::table')
| Moderated By:        |
| :-------------:      |
| Malkie Klaristenfeld |
@endcomponent
@component('mail::table')
| Special Guest Speaker |
|:-------------:        |
| Dr. David Fox         |
@endcomponent

<div class="table" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
<table style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 30px auto; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
<thead style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;"><tr>
<th style="width: 40%; font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding-bottom: 8px; margin: 0; text-align: center;"></th>
<th style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding-bottom: 8px; padding-right: 12px; margin: 0; text-align: right;">To register and for more information, please text or call Rochel Lea at (917)691-5470, or email at supportgroups@knafayimwings.org</th>
</tr></thead>
<tbody style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;"></tbody>
</table>
</div>

@endcomponent()

<br>
@component('mail::button', ['url' => 'mailto:supportgroups@knafayimwings.org', 'color' => 'd43d32'])
Sign Up
@endcomponent
<br>

@endcomponent()
@endcomponent()

@endcomponent
