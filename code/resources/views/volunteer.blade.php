@extends('layouts.app')

@section('content')
	<hero fade-color="purple" url="/img/cares/boat.jpg">
	</hero>
	<section class="w-full text-center bg-purple-medium text-white py-16 px-4 lg:px-8">
		<h2 class="title text-3xl font-bold relative z-0">Volunteer with Us</h2>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-16 font-bold">
				Mothers who lose a baby at, or shortly after, birth feel isolated in their grief. Jewish law does not mandate the observance of traditional mourning rituals for stillborn babies, which can exacerbate a mother’s loneliness, grief, and depression. To some, the lack of communal customs negates the existence of their child and attempts to sweep their grief under the rug. No woman should shoulder the burden of loss alone.
			</p>
			<p class="mb-16">
Knafayim understands that the amount of time a child has lived does not determine its importance as a human being or as an object of love. We understand that another baby can never replace the child who was lost, and that the parents’ love for their deceased child cannot be “saved” for the future or automatically redistributed among their living children. We are here to understand that every child is a part of their lives and that the relationship didn’t end when the child died, just as the sorrow doesn’t end as soon as the burial is over.
			</p>
		</div>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-16 text-3xl lg:text-4xl text-center">
				Knafayim also understands how important your involvement can be during this painful period of adjustment. There will be few times when you are needed more than you are needed now. There are numerous ways for dedicated volunteers to effectively serve our community and make a direct impact.
			</p>
		</div>
	</section>
	<cta img="/img/cares/sunset.jpg" text-color="black" tagline-color="primary" :light="true" link="#" button-text="SUPPORT" tagline="Discover The Power of Support" align="start">
		<p class="mb-4 text-primary">
			The first step in becoming a Knafayim volunteer is to complete this form. A Knafayim Coordinator will then contact you about attending a volunteer orientation.
		</p>
	</cta>
@endsection
