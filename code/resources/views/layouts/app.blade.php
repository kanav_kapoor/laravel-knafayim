<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- favicons -->
        <link rel="apple-touch-icon" sizes="180x180" href="/img/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/img/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/img/favicons/favicon-16x16.png">
        <link rel="manifest" href="/img/favicons/site.webmanifest">
        <link rel="mask-icon" href="/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

        <title>Knafayim - @yield('title')</title>

        <!-- Fonts -->

        <!-- Styles -->
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app" class="th-font overflow-x-hidden">
            <div class="min-h-screen">
              @include('layouts.header')
              @yield('content')
            </div>
            @include('layouts.footer')
            <back-to-top
              visibleoffset="600"
              text="text"
              bottom="40px"
              right="40px"
            >
              <img class="rounded-full bg-white w-10 h-10 px-2 py-2" src="/img/arrow-up.svg" />
            </back-to-top>
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
