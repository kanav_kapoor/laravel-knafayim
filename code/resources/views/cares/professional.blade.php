@extends('layouts.app')

@section('content')
	<hero fade-color="purple" url="/img/cares/boat.jpg">
	</hero>
	<section class="w-full text-center bg-purple-medium text-white py-16 px-4 lg:px-8">
		<h2 class="title text-3xl font-bold relative z-0">Care for the Professional</h2>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-16 font-bold">
				Many parents have the expectation that professionals are aware of and understand the parental grief process. In reality, most of us have had very little experience regarding bereavement and the very special nature of pregnancy and infant loss.
			</p>
			<p class="mb-16">
				Caregivers do grieve for the death of their patients and often experience shock, isolation, guilt and anxiety. (Some of the same symptoms we warn newly bereaved parents about.)
			</p>
		</div>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-16 text-3xl lg:text-4xl text-center">
To maximize your coping skills, consider the following suggestions:
			</p>
			<p class="mb-8">
				Accept your own feelings. No one is expected to handle every situation that is thrown his way every day.
			</p>
			<p class="mb-8">
				Consider working in teams with loads of support.
			</p>
			<p class="mb-8">
				Consider sending a card or letter to the family or perhaps following up with the patient a few days later. This will provide you with some closure and will be appreciated by the family.
			</p>
			<p class="mb-8">
				Share your feelings with others who understand confidentiality and the grief process.
			</p>
			<p class="mb-8">
				Remind yourself that you did the best you can for the family with the information you had at the time.
			</p>
			<p class="mb-8">
				Seek professional help if you get stuck at any stage in the grief process or you feel unable to carry out the duties of your job over time.
			</p>
		</div>
	</section>
	<cta img="/img/cares/sunset.jpg" text-color="black" tagline-color="primary" :light="true" link="#" button-text="SUPPORT" tagline="Discover The Power of Support" align="start">
		<p class="mb-4 text-primary">
			Feel free to reach out to us. Our trained professionals will be glad to support you.
		</p>
	</cta>
@endsection
