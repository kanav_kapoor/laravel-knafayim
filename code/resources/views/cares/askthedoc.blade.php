@extends('layouts.app')

@section('content')
	<section class="w-full bg-white py-16 px-4 lg:px-8">
		<h2 class="text-center title text-3xl font-bold relative z-0 text-primary mb-4">Ask The Doctor</h2>
    <p class="text-center mb-6">Lorem ipsum you can post your queries and our doctors will respond to you soon.</p>
    <div class="text-center mb-4">
      <kbutton class="cta-button" color="teal">Ask Now</kbutton>
    </div>
    <div class="flex flex-wrap mb-4 max-w-4xl mx-auto">
      <div class="w-screen">
        <section class="m-2 leading-normal">
        <!-- card container -->
        <div class="max-w-lg shadow-lg rounded overflow-hidden m-2 mx-auto">
          <!-- card-content -->
          <accordion-item title="Miscarriage">
          <div class="px-4 py-2 border-b border-grey">
              <h2 class="mb-2 text-primary text-xl">What is Lorem Ipsum?</h2>
              <p class="mb-2 text-grey-dark text-sm">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...
              </p>
              <!-- button -->
              <button class="py-1 px-2 bg-transparent hover:bg-secondary-accent hover:text-white text-secondary-accent border border-secondary-accent font-bold rounded-full mt-1 mb-1 text-xs">
                See Answer
              </button>
            </div>
            <div class="px-4 py-2 border-b border-grey">
              <h2 class="mb-2 text-primary text-xl">What is Lorem Ipsum?</h2>
              <p class="mb-2 text-grey-dark text-sm">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...
              </p>
              <!-- button -->
              <button class="py-1 px-2 bg-transparent hover:bg-secondary-accent hover:text-white text-secondary-accent border border-secondary-accent font-bold rounded-full mt-1 mb-1 text-xs">
                See Answer
              </button>
            </div>
          </accordion-item>
        </div>
        <div class="max-w-lg shadow-lg rounded overflow-hidden m-2 mx-auto">
          <accordion-item title="Infant Loss">
            <div class="px-4 py-2 border-b border-grey">
                <h2 class="mb-2 text-primary text-xl">What is Lorem Ipsum?</h2>
                <p class="mb-2 text-grey-dark text-sm">
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...
                </p>
                <!-- button -->
                <button class="py-1 px-2 bg-transparent hover:bg-secondary-accent hover:text-white text-secondary-accent border border-secondary-accent font-bold rounded-full mt-1 mb-1 text-xs">
                  See Answer
                </button>
              </div>
              <div class="px-4 py-2 border-b border-grey">
                <h2 class="mb-2 text-primary text-xl">What is Lorem Ipsum?</h2>
                <p class="mb-2 text-grey-dark text-sm">
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...
                </p>
                <!-- button -->
                <button class="py-1 px-2 bg-transparent hover:bg-secondary-accent hover:text-white text-secondary-accent border border-secondary-accent font-bold rounded-full mt-1 mb-1 text-xs">
                  See Answer
                </button>
              </div>
            </accordion-item>
        </div>
        </section>
      </div>
      <!-- <div class="w-screen lg:w-1/4"></div> -->
    </div>
	</section>
@endsection
