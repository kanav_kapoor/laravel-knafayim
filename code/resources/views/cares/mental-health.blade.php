@extends('layouts.app')

@section('content')
	<hero url="/img/cares/mental-health.jpg">
	</hero>
	<section class="w-full text-center bg-teal-custom text-white py-16 px-4 lg:px-8">
		<h2 class="title text-3xl font-bold relative z-0">Redefining Strength-Perinatal Mental Health</h2>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-16 font-bold">
				Pregnancy and the arrival of a new baby signal a time of unparalleled change and hope for the future. When this suddenly ends with a loss, for some a traumatic loss, it is a challenging time. Knafayim provides referrals and consultations with mental health professionals to get you on the road to recovery.
			</p>
			<p class="mb-16">
				Let’s redefine your strengths, and let’s not let your challenges define you.
			</p>
			<p class="mb-16">
				Come with me, dear friend, let’s hold hands as we traverse the storm and reach sunnier settings.
			</p>
		</div>
	</section>
	<cta img="/img/cares/waves-beach.jpg" link="#" button-text="FILL FORM" tagline="Discover The Power of Support" align="start">
		For assistance, email us at <span class="text-secondary-accent">redefiningstrength@knafayimwings.org</span>
	</cta>
@endsection
