@extends('layouts.app')

@section('content')
	<hero url="/img/cares/chat-group.jpg">
	</hero>
	<section class="w-full text-center bg-teal-custom text-white py-16 px-4 lg:px-8">
		<h2 class="title text-3xl font-bold relative z-0">Chat Groups</h2>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-16 font-bold">
				Knafayim provides free, 24/6, fully-moderated online support chat groups. Our moderators are fully trained and are also bereaved parents. If you are new to the forums, you’ll need to apply for membership. New members will take 24 to 78 hours to be accepted. All applications are handled by human beings, all volunteer moderators, so please be patient and kind to them!
			</p>
			<p class="mb-16">
				Many Knafayim volunteers are not professional therapists but are trained interventionists and may offer referrals to licensed psychotherapists.
			</p>
			<p class="mb-16">
				Come with me, dear friend, let’s hold hands as we traverse the storm and reach sunnier settings.
			</p>
		</div>
	</section>
	<cta img="/img/cares/tea.jpg" link="#" button-text="FILL FORM" tagline="Discover The Power of Support" align="start">
		To join, please complete the form below, and we will email your membership code when you are accepted.
	</cta>
@endsection
