@extends('layouts.app')

@section('content')
	<hero fade-color="purple" url="/img/cares/rainbow.jpg">
	</hero>
	<section class="w-full text-center bg-purple-medium text-white py-16 px-4 lg:px-8">
		<h2 class="title text-3xl font-bold relative z-0">PAL: Pregnancy After Loss</h2>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-16 font-bold">
				Pregnancy after a loss is a unique experience with conflicting emotions of grief and joy. Knafayim strives to support parents through peers and professionals who understand and validate this complex experience.
			</p>
			<p class="mb-16">
				Knafayim lends DVD hard drives to women laid up during pregnancy. This service is free of charge and is available to all women in time of need. The rights to the hard drives belong to Knafayim, and they may not be duplicated.
			</p>
		</div>
	</section>
	<cta img="/img/cares/tulip.jpg" link="#" button-text="FILL FORM" tagline="Discover The Power of Support" align="start">
		<p class="mb-4">
			<strong>Bedside support</strong> is also available for mothers giving birth after a loss.
		</p>
		<p class="mb-4">
			For support during this time, you can email us at <br /><span class="text-teal-dark font-semibold">pal@knafayimwings.com</span>.
		</p>
		<p>
			To order a DVD hard drive, fill out the form below.
		</p>
	</cta>
@endsection
