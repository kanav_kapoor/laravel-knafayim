@extends('layouts.app')

@section('content')
	<hero fade-color="purple" url="/img/cares/fork.png">
	</hero>
	<section class="w-full text-center bg-purple-medium text-white py-16 px-4 lg:px-8">
		<h2 class="title text-3xl font-bold relative z-0">Pathways</h2>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-16 font-bold">
				Sometimes in life we find ourselves on pathways we never dreamed we would take. Pathways is a post hysterectomy support group for women who underwent emergency hysterectomy. This challenge may be difficult and daunting and Knafayim is here to provide the physical, emotional, and informational support you may need.
			</p>
			<p class="mb-16">
				When you traverse these pathways, you do not have to do it alone. Pathways is your shoulder to lean on when your going gets tough: it is a place where you can be supported and supportive.
			</p>
		</div>
	</section>
	<cta img="/img/cares/rose.jpg" link="#" button-text="FILL FORM" tagline="Discover The Power of Support" align="start">
		To join <strong>Pathways</strong>, submit the form below and a counselor will connect with you within 24 hours.
	</cta>
@endsection
