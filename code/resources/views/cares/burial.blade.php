@extends('layouts.app')

@section('content')
	<hero url="/img/cares/mental-health.jpg">
	</hero>
	<section class="w-full text-center bg-teal-custom text-white py-16 px-4 lg:px-8">
		<h2 class="title text-3xl font-bold relative z-0">Burial Services</h2>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-16 font-bold">
      We hope to alleviate added stress by taking care of the final arrangements for your baby. Our Chevra Kadisha volunteers respectfully handle the details from getting the body released to ensuring a Jewish burial. Burial expenses are arranged at a reduced cost.
			</p>
		</div>
	</section>
	<cta img="/img/cares/waves-beach.jpg" link="#" button-text="FILL FORM" tagline="Discover The Power of Support" align="start">
    To apply, fill out the form, complete and sign the attached document, and someone will contact you shortly.
		For immediate assistance, call (347)678-8588 or email us to support@knafayimwings.org
	</cta>
@endsection
