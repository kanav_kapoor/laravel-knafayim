@extends('layouts.app')

@section('content')
	<hero fade-color="purple" url="/img/cares/lotus.jpg">
	</hero>
	<section class="w-full text-center bg-purple-medium text-white py-16 px-4 lg:px-8">
		<h2 class="title text-3xl font-bold relative z-0">Early Dreams</h2>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-16 font-bold">
				It’s never too early in a pregnancy to miscarry. Knafayim provides just what a woman and her family need when they suspect a pregnancy loss. We create an environment of caring, hope, comfort and healing.
			</p>
			<h3 class="text-primary">
				Early Dreams provides
			<h3>
			<p class="mb-16">
				supportive measures when a woman experiences the end, or the threat to an end of a pregnancy, before 20 weeks of gestation.
			</p>
			<h3 class="text-primary">
				Early Dreams strives
			<h3>
			<p class="mb-16">
				to assess what a miscarriage means to the woman. This holds the key to providing the right care at the right time.
			</p>
			<h3 class="text-primary">
				Early Dreams assists
			<h3>
			<p class="mb-16">
				in the care and treatment of a woman experiencing a molar or ectopic pregnancy.
			</p>
			<h3 class="text-primary">
				Early Dreams honors
			<h3>
			<p class="mb-16">
				the choices and directives of the woman and her family through communication and co-creating care.
			</p>
		</div>
	</section>
	<section class="py-24 text-center bg-secondary-accent text-white">
		<span class="block text-lg lg:text-xl">For immediate assistance, please call</span>
		<span class="block text-xl lg:text-2xl mt-8 font-bold">(917)627-5528</span>
	</section>
	<cta img="/img/cares/water.jpg" text-color="black" tagline-color="primary" :light="true" link="#" button-text="FILL FORM" tagline="Discover The Power of Support" align="start">
		<p class="mb-4 text-primary">
			To join <strong>Early Dreams</strong>, please complete the form below and a counselor will connect with you within 24 hours.
		</p>
	</cta>
@endsection
