@extends('layouts.app')

@section('content')
	<section class="w-full bg-white py-16 px-4 lg:px-8">
		<h2 class="text-center title text-3xl font-bold relative z-0 text-primary mb-4">Holding Hands</h2>
    <p class="text-center mb-12"> The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
    <div class="flex flex-wrap mb-4 max-w-4xl mx-auto">
      <div class="w-screen">
        <section class="flex flex-wrap justify-center m-2 leading-normal">
        <div class="w-full sm:w-1/2 xl:w-1/3 px-1 mb-6">
          <a href="/knafayim-cares/early-dreams" class="flex items-center justify-center linkbox block w-full shadow-lg rounded p-4 bg-cover bg-center h-48">
            <span class="block font-bold underline p-4 rounded-full">Early Dreams (Miscarriage)</span>
          </a>
        </div>
        <div class="w-full sm:w-1/2 xl:w-1/3 px-1 mb-6">
          <a href="/knafayim-cares/heartbeat" class="flex items-center justify-center linkbox block w-full shadow-lg rounded p-4 bg-cover bg-center h-48">
            <span class="block font-bold underline p-4 rounded-full">Heartbeats (Infant Loss)</span>
          </a>
        </div>
        <div class="w-full sm:w-1/2 xl:w-1/3 px-1 mb-6">
          <a href="/knafayim-cares/pathways" class="flex items-center justify-center linkbox block w-full shadow-lg rounded p-4 bg-cover bg-center h-48">
            <span class="block font-bold underline p-4 rounded-full">Pathways (post hysterectomy)</span>
          </a>
        </div>
        <div class="w-full sm:w-1/2 xl:w-1/3 px-1 mb-6">
          <a href="/knafayim-cares/secret" class="flex items-center justify-center linkbox block w-full shadow-lg rounded p-4 bg-cover bg-center h-48">
            <span class="block font-bold underline p-4 rounded-full">Secrets (non viable)</span>
          </a>
        </div>
        <div class="w-full sm:w-1/2 xl:w-1/3 px-1 mb-6">
          <a href="/knafayim-cares/still-a-mother" class="flex items-center justify-center linkbox block w-full shadow-lg rounded p-4 bg-cover bg-center h-48">
            <span class="block font-bold underline p-4 rounded-full">Still a Mother (stillborn)</span>
          </a>
        </div>
        </section>
      </div>
      <!-- <div class="w-screen lg:w-1/3"></div> -->
    </div>
	</section>
@endsection
