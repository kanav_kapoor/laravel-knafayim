@extends('layouts.app')

@section('content')
	<hero fade-color="purple" url="/img/cares/support-group.jpg">
	</hero>
	<section class="w-full text-center bg-purple-medium text-white py-16 px-4 lg:px-8">
		<h2 class="title text-3xl font-bold relative z-0">Support Group</h2>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-16 font-bold">
Knafayim provides monthly, free support groups via teleconference and in various locations around the world. Our trained facilitators and co-facilitators are also bereaved parents themselves, and they will be happy to have you join. The isolation that so often accompanies loss will dissipate, and you will feel a part of the Knafayim family.
			</p>
			<p class="mb-16">
				Twice a year Knafayim hosts medical professionals, including high risk obstetricians and social workers, in a series of teleconferences to address bereaved parents and answer their questions.
			</p>
			<p class="mb-16">
				Twice a year, Knafayim hosts a “Was It For Nothing” in-person evening of remembrance including music, lectures and support.
			</p>
		</div>
	</section>
	<cta img="/img/cares/team.jpg" text-color="black" tagline-color="primary" :light="true" link="#" button-text="FILL FORM" tagline="Discover The Power of Support" align="start">
		<p class="mb-4">
			To join, email us at <span class="text-pr text-primary font-semibold">supportgroups@knafayimwings.org</span>.
		</p>
	</cta>
@endsection
