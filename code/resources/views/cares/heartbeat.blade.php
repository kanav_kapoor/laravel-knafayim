@extends('layouts.app')

@section('content')
	<hero fade-color="purple" url="/img/cares/heartbeats.jpg">
	</hero>
	<section class="w-full text-center bg-purple-medium text-white py-16 px-4 lg:px-8">
		<h2 class="title text-3xl font-bold relative z-0">Heartbeats</h2>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-16 font-bold">
				When a baby dies, suddenly it seems like all meaning has been sucked out of your life. All that was right with the world seems wrong and you’re wondering when, or if, you’ll ever feel better.
			</p>
			<p class="mb-16">
				We’ve been there ourselves and understand some of the pain you are feeling right now. We know that you are trying to find your way through a bewildering experience for which no one can truly be prepared and we are here to help you along that way.
			</p>
		</div>
	</section>
	<cta img="/img/cares/baby-heart.jpg" link="#" button-text="FILL FORM" tagline="Discover The Power of Support" align="start">
		To join <strong>Heartbeats</strong>, submit the form below and a counselor will connect with you within 24 hours.
	</cta>
@endsection
