@extends('layouts.app')

@section('content')
	<hero url="/img/cares/sky-birds.jpg">
	</hero>
	<section class="w-full text-center bg-teal-custom text-white py-16 px-4 lg:px-8">
		<h2 class="title text-3xl font-bold relative z-0">Wings Program</h2>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-16 font-bold">
				Knafayim understands the power of peer support. When two or more people speak the same language even when that language is grief, there is comfort and healing. You can be supported and supportive.
			</p>
			<p class="mb-16">
				Knafayim provides one-on-one support to bereaved parents, grandparents, and siblings for free via our Wings Mentor program. In this program, you will be matched with a trained volunteer, also a bereaved parent, grandparent, or sibling, who will connect with you via phone, email, and/or in person. She will take you under her wings and be there for you during those difficult times until you are ready to fly on your own.
			</p>
		</div>
	</section>
	<cta img="/img/cares/two-cups.jpg" link="#" button-text="FILL FORM" tagline="Discover The Power of Support" align="start">
		To join our <strong>Wings Program</strong>, tell us about yourself and your journey and we will try to connect you with others experiencing similar situations
	</cta>
@endsection
