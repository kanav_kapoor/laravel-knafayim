@extends('layouts.app')

@section('content')
	<hero fade-color="purple" url="/img/cares/secret.jpg">
	</hero>
	<section class="w-full text-center bg-purple-medium text-white py-16 px-4 lg:px-8">
		<h2 class="title text-3xl font-bold relative z-0">Secrets still looking...</h2>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-16 font-bold">
				It’s a secret and we won’t divulge it. Knafayim knows the deep pain of a fatal diagnosis coupled with the stress of keeping it under wraps is too much for a mother to bear. We are here to hold your hand and listen. We are here to guide you through the most painful and difficult choices, and we are with you as you absorb the shockwaves in the aftermath.
			</p>
			<p class="mb-16">
				Secrets is a place where you will be able to share your deepest secrets without the fear of being judged. It’s a place where you can be supported and supportive.
			</p>
		</div>
	</section>
	<section class="py-24 text-center bg-secondary-accent text-white">
		<span class="block text-lg lg:text-xl">For immediate assistance, please call</span>
		<span class="block text-xl lg:text-2xl mt-8 font-bold">(917)627-5528</span>
	</section>
	<cta img="/img/cares/pink-rose.jpg" text-color="black" tagline-color="primary" :light="true" link="#" button-text="FILL FORM" tagline="Discover The Power of Support" align="start">
		<p class="mb-4 text-primary">
			To join <strong>Secrets</strong>, please complete the form below and a counselor will connect with you within 24 hours.
		</p>
	</cta>
@endsection
