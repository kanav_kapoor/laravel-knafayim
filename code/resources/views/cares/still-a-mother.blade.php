@extends('layouts.app')

@section('content')
	<hero url="/img/cares/baby.jpg">
	</hero>
	<section class="w-full text-center bg-teal-custom text-white py-16 px-4 lg:px-8">
		<h2 class="title text-3xl font-bold relative z-0">Still a Mother</h2>
		<div class="w-5/6 lg:w-3/4 mx-auto pt-20 max-w-xl text-basic lg:text-xl leading-normal text-left">
			<p class="mb-16 font-bold">
				Yes, you are still a mother. And your baby is very much a part of your family’s life story, even if it was born perfectly still. The lack of traditional mourning rituals tends to isolate grieving parents. Talking about your child may bring you comfort, yet seems to drive away friends.
			</p>
			<p class="mb-16">
				Knafayim understands how important support, care, and a listening ear are along this journey. Knafayim counselors/volunteers and peers will support you and be that listening ear, as you learn to integrate into society under a new normal.
			</p>
		</div>
	</section>
	<cta img="/img/cares/bear.jpg" link="#" button-text="FILL FORM" tagline="Discover The Power of Support" align="start">
		To join <strong>Still a Mother</strong>, please complete the form below and a counselor will connect with you within 24 hours
	</cta>
@endsection
