<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Rachel Lustigman',
            'email' => 'rlustigman@icloud.com',
            'password' => bcrypt('rechy.5050')
        ]);

        App\User::create([
            'name' => 'Malky Klaristenfeld',
            'email' => 'mklaristenfeld@knafayimwings.org',
            'password' => bcrypt('malky.5528')
        ]);
    }
}
