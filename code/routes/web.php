<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/knafayim-cares', function () {
    return view('cares');
});

Route::get('/knafayim-cares/wings', function () {
    return view('cares.wings');
});

Route::get('/knafayim-cares/still-a-mother', function () {
    return view('cares.still-a-mother');
});

Route::get('/knafayim-cares/heartbeat', function () {
    return view('cares.heartbeat');
});

Route::get('/knafayim-cares/pathways', function () {
    return view('cares.pathways');
});

Route::get('/knafayim-cares/pal', function () {
    return view('cares.pal');
});

Route::get('/knafayim-cares/support-group', function () {
    return view('cares.support-group');
});

Route::get('/knafayim-cares/chat-groups', function () {
    return view('cares.chat-groups');
});

Route::get('/knafayim-cares/mental-health', function () {
    return view('cares.mental-health');
});

Route::get('/knafayim-cares/burial-service', function () {
  return view('cares.burial');
});

Route::get('/knafayim-cares/secret', function () {
    return view('cares.secret');
});

Route::get('/knafayim-cares/professional', function () {
    return view('cares.professional');
});

Route::get('/education/professionals/information', function () {
    return view('education.professional-information');
});

Route::get('/knafayim-cares/early-dreams', function () {
    return view('cares.early-dreams');
});

Route::get('/knafayim-cares/ask-the-doctor', function () {
  return view('cares.askthedoc');
});

Route::get('/knafayim-cares/holding-hands', function () {
  return view('cares.holding-hands');
});

Route::get('/education/professionals/trainings', function () {
    return view('education.trainings');
});

Route::get('/education/families/medical-emergency', function () {
    return view('education.emergency');
});

Route::get('/education/families/information', function () {
    return view('education.information');
});

Route::get('/education/families/medical-terms', function () {
    return view('education.medical-terms');
});

Route::get('/education/families/halacha', function () {
    return view('education.halacha');
});

Route::get('/mens-corner', function () {
    return view('men-and-loss');
});

Route::get('/volunteer', function () {
    return view('volunteer');
});

Route::get('/request-a-package', function(){
    return view('request-a-package');
});


Route::get('/home', 'HomeController@index')->name('home');
