# Installation
1. git clone
2. git checkout dev
3. cd code
4. cp .env.example .env (Fill in database details)
5. composer install
6. sudo chgrp -R www-data storage bootstrap/cache && sudo chmod -R ug+rwx storage bootstrap/cache
7. npm install
8. php artisan key:generate
9. php artisan migrate